**WARNING**
-----------

**Never copy and execute anything from the internet unless you know and
understand exactly what it's doing.**

With that out of the way, this is my custom vimrc. On the first run of vim if
[Vundle](https://github.com/VundleVim/Vundle.vim) isn't installed, the script
will download and install it before running the rest. Any questions or
suggestions please either send me an email directly, open a pull request, or 
create an issue.
