set nocompatible
filetype off

" Install Vundle if not already installed
if empty(glob('~/.vim/bundle/Vundle.vim'))
	silent !git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
endif

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'nightsense/snow' 
Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'scrooloose/nerdcommenter'
Plugin 'vim-airline/vim-airline'
Plugin 'jiangmiao/auto-pairs'

call vundle#end()

" Install plugins before running the rest of the vimrc
if empty(glob('~/.vim/bundle/snow')) || empty(glob('~/.vim/bundle/vim-airline'))
	exe "PluginInstall"
endif

filetype plugin indent on
syntax on

let mapleader=","

set modelines=0

set number

set ruler

set visualbell

set encoding=utf-8

" Whitespace
set wrap
set textwidth=79
set formatoptions=tcqrn1
set tabstop=2
set shiftwidth=2
set softtabstop=2
set noexpandtab
set noshiftround
aug pyindent
	au!
	au FileType python set tabstop=2 softtabstop=2 shiftwidth=2 expandtab
aug end

" Matching and backspace
set scrolloff=5
set backspace=eol,indent,start
set showmatch
set matchpairs+=<:>
runtime! macros/matchit.vim

nnoremap j gj
nnoremap k gk

set hidden

set ttyfast

set laststatus=2

" Show the mode and last command
set showmode
set showcmd

" Searching
nnoremap / /\v
vnoremap / /\v
set hlsearch
set incsearch
set ignorecase
set smartcase
nmap <silent> <leader><space> :let @/=''<cr>
vmap <silent> <leader><space> :let @/=''<cr>

" List Mode
set listchars=tab:»-,eol:¬
nnoremap <silent> <leader>l :set list!<cr>

" Theme and colors
set t_Co=256
set background=dark
colorscheme snow

" Misc.
set cursorline
set wildmenu
set lazyredraw

" Folding
set foldenable
set foldlevelstart=7
nnoremap <space> za
set foldmethod=expr
set foldexpr=GetFoldLevel(v:lnum)

function! GetFoldLevel(lnum)
	let in = len(split(getline(a:lnum), '\v[\{|\[|\(|\<]', 1)) - 1
	let ou = len(split(getline(a:lnum), '\v[\}|\]|\)|\>]', 1)) - 1
	if in > ou
		return "a1" 
	elseif ou > in
		return "s1"
	else
		return -1
	endif
endfunction

" Airline
aug SetAirlineTheme | aug END
au SetAirlineTheme VimEnter * AirlineTheme snow
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = '☰'
let g:airline_symbols.maxlinenr = ''

